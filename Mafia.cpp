#include <iostream>
#include <algorithm>
using namespace std;

int n;

bool f(long long *a, long long c){
long long sum = 0;
for (int i=0; i < n; i++){
sum += c -a[i];
if (sum >= c)
return true;
}
return false;
}

int main(){
cin >> n;
long long a[n];
for (int i = 0; i < n; i++)
cin >> a[i];

sort(a, a + n);

long long r = a[n - 1] /double(n-1) * double(n) + 1;
long long l = a[n-1], c, rez = r;
while (l <= r){
c = (r + l)/2;
if (f(a, c)){
rez = c;
r = c - 1;
}
else {
l = c + 1;
}
}
cout << rez;
return 0;
}
